package com.example.demo;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;
//import java.io.Serial;
import java.io.Serializable;

public class User implements Serializable {
//    @Serial
//    private static final long serialVersionUID = 6529685098267757690L;

    @Min(value = 1, message = "Id should not be less than 1")
    public String id;

    @NotNull(message = "firstname cannot be null")
    public String firstname;
    @NotNull(message = "lastname cannot be null")
    public String lastname;

    @Size(min = 16, max = 16, message
            = "codiceFiscale Me must be between 16 characters")
    public String codiceFiscale;

    public void setId(String valueOf) {
        this.id = valueOf;
    }

    public void setFirstname(String s) {
        this.firstname = s;
    }

    public void setLastname(String s) {
        this.lastname = s;
    }

    public Object getId() {
        return this.id;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

}
