package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepository {

    private static Integer userId = 0;

    public static List<User> getUsers() {
        return users;
    }

    public static void setUsers(List<User> users) {
        UserRepository.users = users;
    }

    public static void createUser(User user) {
        user.setId(String.valueOf(userId++));
        users.add(user);
    }

    public static void addUser(User user) {
        users.add(user);
    }

    private static List<User> users = new ArrayList<>();

    public static boolean removeUser(User user) {
        return users.remove(user);
    }

    public static Optional<User> getUserById(String id) {
        return  users.stream()
                .filter(user -> user.getId().equals(id))
                .findFirst();
    }
}
