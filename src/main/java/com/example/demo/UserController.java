package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping
    public ResponseEntity<List<User>> getusers() {
        return new ResponseEntity<>(UserRepository.getUsers(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<User> createUser(@Valid  @RequestBody User user) {
        UserRepository.createUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PostMapping("/{id}")
    public ResponseEntity<User> modifyUser(@PathVariable String id, @Valid @RequestBody User newuser) {
        Optional<User> olduseroriginal = UserRepository.getUserById(id);
        if (olduseroriginal.isPresent()) {
            User old = olduseroriginal.get();
            UserRepository.removeUser(old);
            newuser.setId(old.id);
            UserRepository.addUser(newuser);
            return new ResponseEntity<>(newuser, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id) {
        return UserRepository.getUserById(id)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}