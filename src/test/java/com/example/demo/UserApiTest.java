package com.example.demo;


import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.testng.annotations.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

class UserApiTest {

    @Test
    void testGetUser() {
        User user = new User();
        user.setId("drimdi97m23z330f");
        user.setCodiceFiscale("drimdi97m23z330f");
        user.setFirstname("imad");
        user.setLastname("diraa");
        UserRepository.addUser(user);
        assertNotEquals(UserRepository.getUsers().stream().findFirst(), user);
    }

    @Test
    void testUserValidate() {
        User user = new User();
        user.setId("drimdi97m23z330f");
        user.setCodiceFiscale("drimdi97m23z330f");
        user.setFirstname("imad");
        user.setLastname("diraa");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertNotEquals(violations.isEmpty(), true);
    }
}